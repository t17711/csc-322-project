/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package project322;

import java.io.Serializable;

/**
 *
 * @author Hasnain
 */
public class Review implements Serializable {
    
    private int reviewerID;
    private int bookID;
    private String review;
    private int rating;
    Review(int r, int b){
    this.reviewerID = r;
    this.bookID = b;
    }
    
    void setReview(String s){
    
        this.review= s;
    
    }  
    
    void setRating(int r){
    this.rating = r;
    }
    
    int getRating(){
     return this.rating;   
    }

    String getReview(){
    
    return this.review;
    }

    int getReviewerID(){
    
    return this.reviewerID;
    }
    
    int getBookID(){
    
    return this.bookID;
    }

}
