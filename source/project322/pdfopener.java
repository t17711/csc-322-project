/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project322;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
import org.icepdf.ri.common.ComponentKeyBinding;
import org.icepdf.ri.common.SwingController;
import org.icepdf.ri.common.SwingViewBuilder;

/**
 *
 * 15: *
 *
 * 16: * This is where the pdf is opened and timed as well on the jpanel
 
 * 17:
 */
public class pdfopener extends JPanel {

/*
String Path = the path for pdf file
Int Points = Points of the User
Int Counter = after the counter reaches a certain value it starts to subtract points at slower speeds
goBack = is the back button, back to the home screen
clock= show much time has passed
book = data time book
Timer t = is used time the pdf depending on the points
isShared= if true subtracts the points half the speed since it is shared

*/

    String path;
    int points;
    int counter;
    public JButton goBack = new JButton("go back");
    public JLabel clock = new JLabel("Time left: ");
    ArrayList<User> users = new ArrayList();
    ArrayList<Suser> susers = new ArrayList();
    Book book;
    boolean isShared = false;
    Timer t = new Timer(500, null);
    int x = 1;
    Gui base;
    int uid;
    
    
    pdfopener(String File, int uid, Gui base, bookInfoGui B,Book book, boolean Sharing ) throws URISyntaxException, IOException, ClassNotFoundException {
        this.base=base;
        base.login_area.setVisible(false);
        this.uid = uid;
        this.book=book;
        this.path = "BookFiles/" + File;
        if (base.suser){
            susers = Data.importSUserDB();
            this.points =susers.get(uid).getPoints();
        }
        else{
            users=Data.importUserDB();
            this.points =users.get(uid).getPoints();
        }
        this.counter = 0;
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        clock.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
        if(Sharing == true)
        {
            x = 2;
            t.setDelay(x*500);
        }
        
   // build a controller
        SwingController controller = new SwingController();

   // Build a SwingViewFactory configured with the controller
        SwingViewBuilder factory = new SwingViewBuilder(controller);
        JPanel viewerComponentPanel = factory.buildViewerPanel();

  // add copy keyboard command
        ComponentKeyBinding.install(controller, viewerComponentPanel);

  // add interactive mouse link annotation support via callback
        controller.getDocumentViewController().setAnnotationCallback(new org.icepdf.ri.common.MyAnnotationCallback(controller.getDocumentViewController()));
        this.add(goBack);
        this.add(clock);
        
   // Create a JFrame to display the panel in
        this.add(viewerComponentPanel);

   // Open a PDF document to view
        
        goBack.addActionListener((ActionEvent w) -> {
            t.stop();
                try {
                    stop(controller , B);
                } catch (IOException ex) {
                    Logger.getLogger(pdfopener.class.getName()).log(Level.SEVERE, null, ex);
                }
        });
        
        ActionListener a = (ActionEvent e) -> {
        //sets the text for the points and remaining and time used
        
            clock.setText("Time Spent :" + counter + " \tPoints left " + points);
            base.loggedIn.points.setText("Points: "+ points+"  ");
            clock.repaint();           
            if (points == 0) {
                t.stop();
                try {
                    stop(controller,B);
                } catch (IOException ex) {
                    Logger.getLogger(pdfopener.class.getName()).log(Level.SEVERE, null, ex);
                }
                // close
                //how the points get deducted
                if (base.suser)
                    susers.get(uid).addPoints(-1 * susers.get(uid).getPoints());
                              
                else
                    users.get(uid).addPoints(-1 * users.get(uid).getPoints());
                t.stop();
            } else if (counter == 1) {
                t.stop();
                t.setDelay(x*1000);
                t.start();
                
            } else if (counter == 2) {
                t.stop();
                t.setDelay(x*1500);
                t.start();             
            }
            counter++;
            points--;
        }; //To change body of generated methods, choose Tools | Templates.
        
        t.addActionListener(a);
        controller.openDocument(path);
        t.start();
    }
    //constructor for pdf opener
    pdfopener(String File, int uid, Gui base, shareBookInfo B, Book book, boolean Sharing) {
     this.base=base;
        base.login_area.setVisible(false);
        this.uid = uid;
        this.book=book;
        this.path = "BookFiles/" + File;
        if (base.suser){
         try {
             susers = Data.importSUserDB();
             this.points =susers.get(uid).getPoints();
         } catch (IOException ex) {
             Logger.getLogger(pdfopener.class.getName()).log(Level.SEVERE, null, ex);
         } catch (ClassNotFoundException ex) {
             Logger.getLogger(pdfopener.class.getName()).log(Level.SEVERE, null, ex);
         }
        }
        else{
         try {
             users=Data.importUserDB();
             this.points =users.get(uid).getPoints();
         } catch (IOException | ClassNotFoundException ex) {
             Logger.getLogger(pdfopener.class.getName()).log(Level.SEVERE, null, ex);
         }
        }
        this.counter = 0;
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        clock.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
        if(Sharing == true)
        {
            x = 2;
            t.setDelay(x*500);
        }
        
   // build a controller
        SwingController controller = new SwingController();

   // Build a SwingViewFactory configured with the controller
        SwingViewBuilder factory = new SwingViewBuilder(controller);
        JPanel viewerComponentPanel = factory.buildViewerPanel();

  // add copy keyboard command
        ComponentKeyBinding.install(controller, viewerComponentPanel);

  // add interactive mouse link annotation support via callback
        controller.getDocumentViewController().setAnnotationCallback(new org.icepdf.ri.common.MyAnnotationCallback(controller.getDocumentViewController()));
        this.add(goBack);
        this.add(clock);
        
   // Create a JFrame to display the panel in
        this.add(viewerComponentPanel);

   // Open a PDF document to view
        
        goBack.addActionListener((ActionEvent w) -> {
            t.stop();
         try {
             stop(controller , B);
         } catch (IOException ex) {
             Logger.getLogger(pdfopener.class.getName()).log(Level.SEVERE, null, ex);
         }
        });

        ActionListener a = (ActionEvent e) -> {
            clock.setText("Time Spent :" + counter + " \tPoints left " + points);
            base.loggedIn.points.setText("Points: "+ points+"  ");
            clock.repaint();           
            if (points == 0) {
                t.stop();
                try {
                    stop(controller,B);
                } catch (IOException ex) {
                    Logger.getLogger(pdfopener.class.getName()).log(Level.SEVERE, null, ex);
                }
                // close
                if (base.suser)
                    susers.get(uid).addPoints(-1 * susers.get(uid).getPoints());
                              
                else
                    users.get(uid).addPoints(-1 * users.get(uid).getPoints());
                t.stop();
            } else if (counter == 1) {
                t.stop();
                t.setDelay(x*1000);
                t.start();
                
            } else if (counter == 2) {
                t.stop();
                t.setDelay(x*1500);
                t.start();             
            }
            counter++;
            points--;
        }; //To change body of generated methods, choose Tools | Templates.
        
        t.addActionListener(a);
        controller.openDocument(path);
        t.start();
    }
    
    private void stop(SwingController controller, bookInfoGui B) throws IOException{
        base.login_area.setVisible(true);
        if (base.suser){
                susers.get(uid).addPoints((-1) * susers.get(uid).getPoints() - points);
                susers.get(uid).addToHistory(book);
                Data.UpdateSUserDB(susers);               
        }
        else{
            users.get(uid).addPoints((-1) * users.get(uid).getPoints() - points);
                users.get(uid).addToHistory(book);
                Data.UpdateUserDB(users);
        }
            controller.closeDocument();
            base.books.removeAll();
            base.books.add(B);
            base.revalidate();
            base.repaint();
    }
    private void stop(SwingController controller, shareBookInfo B) throws IOException{
        base.login_area.setVisible(true);
        if (base.suser){
                susers.get(uid).addPoints((-1) * susers.get(uid).getPoints() - points);
                susers.get(uid).addToHistory(book);
                Data.UpdateSUserDB(susers);               
        }
        else{
            users.get(uid).addPoints((-1) * users.get(uid).getPoints() - points);
                users.get(uid).addToHistory(book);
                Data.UpdateUserDB(users);
        }
            controller.closeDocument();
            base.books.removeAll();
            base.books.add(B);
            base.revalidate();
            base.repaint();
    }
  }
