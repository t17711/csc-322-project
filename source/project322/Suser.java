package project322;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import java.util.ArrayList;

/**
 *
 * @author Max
 */
public class Suser extends User {
    // has all user property plus some extra function
    public Suser(int uniqueID, String pword, String fname, String lname) {
        this.uid = uniqueID;
        this.password = pword;
        this.fName = fname;
        this.lName = lname;
    }

    public static void addPoints(int uid, boolean suser) throws IOException, ClassNotFoundException {
    // add poimnts to user and updates db
        if (suser) {
            ArrayList<Suser> su = Data.importSUserDB();
            su.get(uid).addPoints(30);
            Data.UpdateSUserDB(su);
        } else {
            ArrayList<User> u = Data.importUserDB();
            u.get(uid).addPoints(30);
            Data.UpdateUserDB(u);
        }
    }

    public static void approveUser(User u) throws IOException, ClassNotFoundException {
        // add user to user and updates db
        ArrayList<User> us = Data.importUserDB();
        us.add(u);
        Data.UpdateUserDB(us);
        popUser();
    }

    public static void DisApproveUser() throws IOException, ClassNotFoundException {
        // add null to uid  and updates db, so that we can make uids consistant, there is no user for this uid
        ArrayList<User> us = Data.importUserDB();
        User u;
        u = null;
        us.add(u);
        Data.UpdateUserDB(us);
        popUser();

    }
    private static void popUser() throws IOException, ClassNotFoundException{
    // removes top pending user
        ArrayList<User> u= Data.importPendUserDB();
        if (u.size()>0)
            u.remove(0);
        Data.UpdatePendUserDB(u);
    }
    public static void disapproveBook(Book b) throws IOException, ClassNotFoundException {
    // remove book from pend books
        ArrayList<Book> bk;
        bk = Data.importPendBookDB();
        for (int i = 0; i < bk.size(); ++i) {
            if (bk.get(i).getTitle() == null ? b.getTitle() == null : bk.get(i).getTitle().equals(b.getTitle())) {
                bk.remove(i);
                break;
            }
        }
       Data.UpdatePendBookDB(bk);

    }
    
    public static void approveBook(Book b) throws IOException, ClassNotFoundException {
    // add pending book to book db and copy files to book folders
        ArrayList<Book> bks = Data.importBookDB();
        bks.add(b);  // add book
        addPoints(b.getContributer(), b.isSuser());  // add point
        Data.UpdateBookDB(bks);
        Files.copy(Paths.get("PendBookFiles/" +b.getFile()),Paths.get("BookFiles//" +b.getFile()),REPLACE_EXISTING);
        Files.copy(Paths.get("PendBookCover/" +b.getCover()),Paths.get("BookCover//" +b.getCover()),REPLACE_EXISTING);
        disapproveBook(b);
        
    }
    
    public static void approveComplaint(int book) throws IOException, ClassNotFoundException {
        ArrayList<Book> bks = Data.importBookDB();
        bks.get(book).setcomplaintCount();
        if (bks.get(book).getcomplaintCount() > 3) {
            bks.remove(book);
        }
        Data.UpdateBookDB(bks);
    }
}
