
package project322;

import java.io.Serializable;
import java.util.*;
import javax.swing.JPanel;

/**
 *
 * @author Max
 */
public class User implements Serializable{
    protected int uid;
    
    protected String password;
    protected String fName;
    protected String lName;
    protected int bannedBookCount;
    protected ArrayList<Book> recommended = new ArrayList<Book>(5);
    protected ArrayList<Book> history = new ArrayList<Book>(5);
    protected ArrayList<Book> favorites = new ArrayList<Book>(5);
    protected ArrayList<Book> shared = new ArrayList<Book>(5);
    protected ArrayList<String> badwords = new ArrayList<String>(5);
    protected int points=50;
    
    
    // read, submit, review, search for bad words, invite, check reading history
    public User(){
    uid=0;
    password="";
    
    }
    public User(int uniqueID, String pword, String fname, String lname){
        this.uid=uniqueID;
        this.password=pword;
        this.fName=fname;
        this.lName=lname;
        
        
    }
    
    //public void setUid(int nuid){this.uid=nuid;}
    public void setPword(String npw){this.password=npw;}
    public void setFname(String nfn){this.fName=nfn;}
    public void setLname(String nln){this.lName=nln;}
    public void addPoints(int nPoints){this.points+=nPoints;}
    public void addToRecommended(Book b){
    this.recommended.add(b);
    }
    public void addToHistory(Book b){
    this.history.add(b);
    }
    public void addToFavorites(Book b){
    this.favorites.add(b);
    }
    public void addToShared(Book b){
    this.shared.add(b);
    }
    public int getUid(){return this.uid;}
    public String getPword(){return this.password;}
    public String getFname(){return this.fName;}
    public String getLname(){return this.lName;}
    public int getPoints(){return this.points;}
    public ArrayList<Book> getRecommended(){return recommended;}
    public ArrayList<Book> getHistory(){return history;}
    public ArrayList<Book> getFavorites(){return favorites;}
    public ArrayList<Book> getShared(){return shared;}
    public void addBadWord(String s){
    this.badwords.add(s);
    }

    public void addBannedBook(){this.bannedBookCount++;}
  
    public int getBannedBookCount(){return this.bannedBookCount;}
}
